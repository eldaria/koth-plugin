package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class SetReward {
   P plugin;

   public SetReward(P var1) {
      this.plugin = var1;
   }

   public void SET_REWARD(Player var1, String var2) {
      if (!var1.hasPermission("KOTH.REWARDTYPE") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else if (!var2.equalsIgnoreCase("INVENTORY") && !var2.equalsIgnoreCase("KEY")) {
         if (var2.equalsIgnoreCase("NONE")) {
            this.plugin.settings.getConfig().set("REWARDS.USE_REWARDS", Boolean.valueOf(false));
            this.plugin.settings.getConfig().set("REWARDS.REWARD_TYPE", "NONE");
            this.plugin.settings.saveConfig();
            var1.sendMessage(ChatColor.GREEN + "The reward type is now set to NONE.");
         } else {
            var1.sendMessage(ChatColor.RED + "Current Reward Types: KEY, INVENTORY, NONE");
         }
      } else {
         this.plugin.settings.getConfig().set("REWARDS.USE_REWARDS", Boolean.valueOf(true));
         this.plugin.settings.getConfig().set("REWARDS.REWARD_TYPE", var2.toUpperCase());
         this.plugin.settings.saveConfig();
         var1.sendMessage(ChatColor.GREEN + "The reward type is now set to " + var2.toUpperCase() + ".");
      }

   }
}
