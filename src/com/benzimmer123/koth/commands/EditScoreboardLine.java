package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.entity.Player;

public class EditScoreboardLine {
   P plugin;

   public EditScoreboardLine(P var1) {
      this.plugin = var1;
   }

   public void REMOVE_LINE(Player var1, String var2) {
      if (!var1.hasPermission("KOTH.EDITSCOREBOARD") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      }

   }

   public void ADD_LINE(Player var1, String var2) {
      if (!var1.hasPermission("KOTH.EDITSCOREBOARD") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      }

   }
}
