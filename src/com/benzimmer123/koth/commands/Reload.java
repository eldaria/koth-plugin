package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Reload {
   P plugin;

   public Reload(P var1) {
      this.plugin = var1;
   }

   public void RELOAD(Player var1) {
      if (!var1.hasPermission("KOTH.RELOAD") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else {
         this.plugin.settings.setup(this.plugin);
         var1.sendMessage(ChatColor.GREEN + "KOTH has been reloaded.");
      }

   }
}
