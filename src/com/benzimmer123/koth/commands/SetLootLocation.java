package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class SetLootLocation {
   P plugin;

   public SetLootLocation(P var1) {
      this.plugin = var1;
   }

   public void SET_LOOT_LOCATION(Player var1) {
      if (!var1.hasPermission("KOTH.SETLOOTLOCATION") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else {
         Block var2 = var1.getTargetBlock((HashSet)null, 100);
         int var3 = 1;
         if (this.plugin.settings.getConfig().getConfigurationSection("LOOT_LOCATION") != null) {
            for(String var4 : this.plugin.settings.getConfig().getConfigurationSection("LOOT_LOCATION").getKeys(false)) {
               ++var3;
            }
         }

         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var3 + ".WORLD", var2.getWorld().getName());
         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var3 + ".X", Integer.valueOf(var2.getX()));
         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var3 + ".Y", Integer.valueOf(var2.getY()));
         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var3 + ".Z", Integer.valueOf(var2.getZ()));
         this.plugin.settings.saveConfig();
         var1.sendMessage(ChatColor.GREEN + "Loot location has been set.");
      }

   }
}
