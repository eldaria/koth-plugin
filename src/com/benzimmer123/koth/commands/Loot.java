package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import com.benzimmer123.koth.listeners.RewardsSystem;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class Loot {
   P plugin;

   public Loot(P var1) {
      this.plugin = var1;
   }

   public void LOOT(Player var1) {
      if (!var1.hasPermission("KOTH.VIEWLOOT") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else {
         Inventory var2 = Bukkit.createInventory((InventoryHolder)null, (new RewardsSystem(this.plugin)).InventoryViewSize(), this.plugin.settings.getConfig().getString("REWARDS.VIEW_INVENTORY_NAME").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&k", "§k").replaceAll("&r", "§r").replaceAll("&n", "§n").replaceAll("&m", "§m"));

         for(int var3 = 0; var3 < var2.getSize(); ++var3) {
            if (this.plugin.settings.getLoot().isSet("LOOT_ITEMS." + var3)) {
               ItemStack var4 = this.plugin.settings.getLoot().getItemStack("LOOT_ITEMS." + var3);
               var2.setItem(var3, var4);
            }
         }

         var1.openInventory(var2);
      }

   }
}
