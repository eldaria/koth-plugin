package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class DisableScoreboard {
   P plugin;

   public DisableScoreboard(P var1) {
      this.plugin = var1;
   }

   public void DISABLE(Player var1, String var2) {
      if (!var1.hasPermission("KOTH.DISABLESCOREBOARD") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else {
         Object var3;
         if (!this.plugin.settings.getConfig().contains("SCOREBOARD.DISABLE_IN_WORLD")) {
            var3 = new ArrayList();
         } else {
            var3 = this.plugin.settings.getConfig().getList("SCOREBOARD.DISABLE_IN_WORLD");
         }

         if (((java.util.List)var3).contains(var2.toUpperCase())) {
            var1.sendMessage(ChatColor.RED + "This world has already been disabled.");
         } else {
            ((java.util.List)var3).add(var2.toUpperCase());
            this.plugin.settings.getConfig().set("SCOREBOARD.DISABLE_IN_WORLD", var3);
            this.plugin.settings.saveConfig();
            var1.sendMessage(ChatColor.GREEN + "You have disabled the scoreboard in " + var2 + ".");
         }
      }

   }
}
