package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RemoveLootLocation {
   P plugin;

   public RemoveLootLocation(P var1) {
      this.plugin = var1;
   }

   public void REMOVE_LOOT_LOCATION(Player var1, String var2) {
      if (!var1.hasPermission("KOTH.REMOVELOOTLOCATION") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else if (this.plugin.settings.getConfig().isSet("LOOT_LOCATION." + var2 + ".WORLD") && this.plugin.settings.getConfig().isSet("LOOT_LOCATION." + var2 + ".X") && this.plugin.settings.getConfig().isSet("LOOT_LOCATION." + var2 + ".Y") && this.plugin.settings.getConfig().isSet("LOOT_LOCATION." + var2 + ".Z")) {
         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var2 + ".WORLD", (Object)null);
         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var2 + ".X", (Object)null);
         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var2 + ".Y", (Object)null);
         this.plugin.settings.getConfig().set("LOOT_LOCATION." + var2 + ".Z", (Object)null);
         this.plugin.settings.saveConfig();
         var1.sendMessage(ChatColor.GREEN + "Loot location " + var2 + " has been removed.");
      } else {
         var1.sendMessage(ChatColor.RED + "There is no such loot location with this id.");
      }

   }

   public void REMOVE_ALL_LOOT_LOCATION(Player var1) {
      if (!var1.hasPermission("KOTH.REMOVELOOTLOCATION") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else {
         this.plugin.settings.getConfig().set("LOOT_LOCATION", (Object)null);
         var1.sendMessage(ChatColor.GREEN + "All loot locations have been reset.");
      }

   }
}
