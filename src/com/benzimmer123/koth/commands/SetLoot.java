package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class SetLoot {
   P plugin;

   public SetLoot(P var1) {
      this.plugin = var1;
   }

   public void SET_LOOT(Player var1) {
      if (!var1.hasPermission("KOTH.SETLOOT") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else {
         Inventory var2 = Bukkit.createInventory((InventoryHolder)null, 54, "KOTH Rewards");
         var2.clear();

         for(int var3 = 0; var3 < 55; ++var3) {
            if (this.plugin.settings.getLoot().isSet("LOOT_ITEMS." + var3)) {
               ItemStack var4 = this.plugin.settings.getLoot().getItemStack("LOOT_ITEMS." + var3);
               var2.setItem(var3, var4);
            }
         }

         var1.openInventory(var2);
      }

   }
}
