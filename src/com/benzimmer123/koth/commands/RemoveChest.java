package com.benzimmer123.koth.commands;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class RemoveChest {
   P plugin;

   public RemoveChest(P var1) {
      this.plugin = var1;
   }

   public void REMOVE_CHEST(Player var1) {
      if (!var1.hasPermission("KOTH.REMOVECHEST") && !var1.hasPermission("KOTH.*") && !var1.isOp()) {
         var1.sendMessage((new MessageConverter(this.plugin)).NoPermissions());
      } else {
         Block var2 = var1.getTargetBlock((HashSet)null, 100);
         if (var2 == null) {
            var1.sendMessage(ChatColor.RED + "You must be looking a block.");
         } else if (this.plugin.settings.getConfig().getConfigurationSection("LOOT_LOCATION") != null) {
            for(String var3 : this.plugin.settings.getConfig().getConfigurationSection("LOOT_LOCATION").getKeys(false)) {
               if (this.plugin.settings.getConfig().getString("LOOT_LOCATION." + var3 + ".WORLD") == var2.getWorld().getName() && (double)this.plugin.settings.getConfig().getInt("LOOT_LOCATION." + var3 + ".X") == var2.getLocation().getX() && (double)this.plugin.settings.getConfig().getInt("LOOT_LOCATION." + var3 + ".Y") == var2.getLocation().getY() && (double)this.plugin.settings.getConfig().getInt("LOOT_LOCATION." + var3 + ".Z") == var2.getLocation().getZ()) {
                  this.plugin.settings.getConfig().set("LOOT_LOCATION." + var3, (Object)null);
               }
            }
         }
      }

   }
}
