package com.benzimmer123.koth;

import com.benzimmer123.koth.listeners.Manager;
import org.bukkit.Bukkit;

public class KOTHTasks {
   P plugin;

   public KOTHTasks(P var1) {
      this.plugin = var1;
   }

   public void callKOTHTask() {
      Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this.plugin, () -> {
		 (new Manager(KOTHTasks.this.plugin)).MinusMaxRunTime();
		 (new Manager(KOTHTasks.this.plugin)).MinusChatDelay();
		 (new Manager(KOTHTasks.this.plugin)).checkKOTHS();
	  }, 20L, 20L);
   }
}
