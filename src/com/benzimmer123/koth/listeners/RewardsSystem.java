package com.benzimmer123.koth.listeners;

import com.benzimmer123.koth.MessageConverter;
import com.benzimmer123.koth.P;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class RewardsSystem implements Listener {
   P plugin;
   ArrayList items = new ArrayList();

   public RewardsSystem(P var1) {
      this.plugin = var1;
   }

   public int InventorySize() {
      int var1 = this.plugin.settings.getConfig().getInt("MAX_LOOT_ITEMS");
      return var1 > 27 ? 54 : 27;
   }

   public int InventoryViewSize() {
      return this.plugin.settings.getConfig().isSet("REWARDS.CHEST_SIZE") ? this.plugin.settings.getConfig().getInt("REWARDS.CHEST_SIZE") : 54;
   }

   public void kothCapped(Player var1) {
      if (this.plugin.settings.getConfig().getBoolean("REWARDS.USE_REWARDS")) {
         if (this.plugin.settings.getConfig().getString("REWARDS.REWARD_TYPE").equalsIgnoreCase("KEY")) {
            ItemStack var2 = new ItemStack(this.plugin.settings.getConfig().getInt("REWARDS.ITEM_ID"));
            ItemMeta var3 = var2.getItemMeta();
            var3.setDisplayName(this.plugin.settings.getConfig().getString("REWARDS.ITEM_DISPLAY_NAME").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&n", "§n").replaceAll("&r", "§r").replaceAll("&k", "§k").replaceAll("&m", "§m"));
            var3.setLore(Arrays.asList(this.plugin.settings.getConfig().getString("REWARDS.ITEM_LORE").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&n", "§n").replaceAll("&r", "§r").replaceAll("&k", "§k").replaceAll("&m", "§m")));
            var2.setItemMeta(var3);
            if (this.getInventorySpace(var1.getInventory())) {
               var1.getInventory().addItem(new ItemStack[]{var2});
            } else {
               var1.getWorld().dropItemNaturally(var1.getLocation(), var2);
               var1.sendMessage((new MessageConverter(this.plugin)).InventoryFull());
            }
         } else if (this.plugin.settings.getConfig().getString("REWARDS.REWARD_TYPE").equalsIgnoreCase("INVENTORY")) {
            this.addItems(var1.getInventory(), "INVENTORY", var1);
         }
      }

   }

   public void addItems(Inventory var1, String var2, Player var3) {
      if (this.plugin.settings.getLoot().isSet("LOOT_ITEMS")) {
         this.items.clear();

         for(String var4 : this.plugin.settings.getLoot().getConfigurationSection("LOOT_ITEMS").getKeys(false)) {
            this.items.add(this.plugin.settings.getLoot().getItemStack("LOOT_ITEMS." + var4));
         }

         boolean var11 = this.plugin.settings.getConfig().getBoolean("REWARDS.DUPLICATE_LOOT");
         int var12 = this.plugin.settings.getConfig().getInt("REWARDS.MAX_LOOT_ITEMS");
         int var6 = this.plugin.settings.getConfig().getInt("REWARDS.MIN_LOOT_ITEMS");
         int var7 = this.randInt(var6, var12);
         int var8 = 0;

         while(var8 < var7 && this.items.size() > 0) {
            if (var2.equalsIgnoreCase("CHEST")) {
               ItemStack var9 = (ItemStack)this.items.get((new Random()).nextInt(this.items.size()));
               int var10 = (new Random()).nextInt(var1.getSize());
               var1.setItem(var10, var9);
               ++var8;
               if (!var11) {
                  this.items.remove(var9);
               }
            } else {
               ItemStack var13 = (ItemStack)this.items.get((new Random()).nextInt(this.items.size()));
               if (this.getInventorySpace(var1)) {
                  var1.addItem(new ItemStack[]{var13});
                  ++var8;
               } else {
                  var3.getWorld().dropItemNaturally(var3.getLocation(), var13);
                  ++var8;
               }

               if (!var11) {
                  this.items.remove(var13);
               }
            }
         }
      }

   }

   public boolean getInventorySpace(Inventory var1) {
      int var2 = 0;

      for(ItemStack var3 : var1.getContents()) {
         if (var3 == null) {
            ++var2;
         }
      }

      if (var2 > 0) {
         return true;
      } else {
         return false;
      }
   }

   public int randInt(int var1, int var2) {
      Random var3 = new Random();
      int var4 = var3.nextInt(var2 - var1 + 1) + var1;
      return var4;
   }

   @EventHandler
   public void PlayerInteract(PlayerInteractEvent var1) {
      ArrayList var2 = new ArrayList();
      var2.clear();
      if (this.plugin.settings.getConfig().getConfigurationSection("LOOT_LOCATION") != null) {
         for(String var3 : this.plugin.settings.getConfig().getConfigurationSection("LOOT_LOCATION").getKeys(false)) {
            if (this.plugin.settings.getConfig().getString("LOOT_LOCATION." + var3) != null) {
               String var5 = this.plugin.settings.getConfig().getString("LOOT_LOCATION." + var3 + ".WORLD");
               int var6 = this.plugin.settings.getConfig().getInt("LOOT_LOCATION." + var3 + ".X");
               int var7 = this.plugin.settings.getConfig().getInt("LOOT_LOCATION." + var3 + ".Y");
               int var8 = this.plugin.settings.getConfig().getInt("LOOT_LOCATION." + var3 + ".Z");
               World var9 = Bukkit.getWorld(var5);
               Location var10 = new Location(var9, (double)var6, (double)var7, (double)var8);
               var2.add(var10);
            }
         }
      }

      if (var1.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
         if (var2.contains(var1.getClickedBlock().getLocation())) {
            var1.setCancelled(true);
            ItemStack var11 = new ItemStack(this.plugin.settings.getConfig().getInt("REWARDS.ITEM_ID"));
            ItemMeta var13 = var11.getItemMeta();
            var13.setDisplayName(this.plugin.settings.getConfig().getString("REWARDS.ITEM_DISPLAY_NAME").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&n", "§n").replaceAll("&r", "§r").replaceAll("&k", "§k").replaceAll("&m", "§m"));
            var13.setLore(Arrays.asList(this.plugin.settings.getConfig().getString("REWARDS.ITEM_LORE").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&n", "§n").replaceAll("&r", "§r").replaceAll("&k", "§k").replaceAll("&m", "§m")));
            var11.setItemMeta(var13);
            if (var1.getPlayer().getItemInHand().getTypeId() == this.plugin.settings.getConfig().getInt("REWARDS.ITEM_ID") && var1.getPlayer().getItemInHand().getItemMeta().getDisplayName() != null && var1.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(this.plugin.settings.getConfig().getString("REWARDS.ITEM_DISPLAY_NAME").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&n", "§n").replaceAll("&r", "§r").replaceAll("&k", "§k").replaceAll("&m", "§m")) && var1.getPlayer().getItemInHand().getItemMeta().getLore().get(0) != null && ((String)var1.getPlayer().getItemInHand().getItemMeta().getLore().get(0)).equals(this.plugin.settings.getConfig().getString("REWARDS.ITEM_LORE").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&n", "§n").replaceAll("&r", "§r").replaceAll("&k", "§k").replaceAll("&m", "§m"))) {
               Inventory var15 = Bukkit.createInventory(var1.getPlayer(), this.InventorySize(), this.plugin.settings.getConfig().getString("REWARDS.REAL_INVENTORY_NAME").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&k", "§k").replaceAll("&r", "§r").replaceAll("&n", "§n").replaceAll("&m", "§m"));
               this.addItems(var15, "CHEST", (Player)null);
               var1.getPlayer().openInventory(var15);
               int var17 = var1.getPlayer().getItemInHand().getAmount();
               if (var17 > 1) {
                  var1.getPlayer().getItemInHand().setAmount(var17 - 1);
                  var1.getPlayer().setItemInHand(var1.getPlayer().getItemInHand());
               } else {
                  var1.getPlayer().setItemInHand(new ItemStack(Material.AIR));
               }

               var1.getPlayer().updateInventory();
            } else {
               var1.getPlayer().sendMessage((new MessageConverter(this.plugin)).NoKey());
            }
         }
      } else if (var1.getAction().equals(Action.LEFT_CLICK_BLOCK) && var2.contains(var1.getClickedBlock().getLocation()) && this.plugin.settings.getConfig().getBoolean("REWARDS.LEFT_CLICK_SHOW_LOOT") && (var1.getPlayer().hasPermission("KOTH.VIEWLOOT") || var1.getPlayer().hasPermission("KOTH.*") || var1.getPlayer().isOp())) {
         Inventory var12 = Bukkit.createInventory((InventoryHolder)null, (new RewardsSystem(this.plugin)).InventoryViewSize(), this.plugin.settings.getConfig().getString("REWARDS.VIEW_INVENTORY_NAME").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&k", "§k").replaceAll("&r", "§r").replaceAll("&n", "§n").replaceAll("&m", "§m"));

         for(int var14 = 0; var14 < var12.getSize(); ++var14) {
            if (this.plugin.settings.getLoot().isSet("LOOT_ITEMS." + var14)) {
               ItemStack var16 = this.plugin.settings.getLoot().getItemStack("LOOT_ITEMS." + var14);
               var12.setItem(var14, var16);
            }
         }

         var1.getPlayer().openInventory(var12);
      }

   }
}
