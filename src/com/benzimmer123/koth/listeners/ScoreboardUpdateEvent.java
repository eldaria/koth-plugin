package com.benzimmer123.koth.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ScoreboardUpdateEvent extends Event {
   String capper;
   String koth;
   String world;
   String faction;
   String timeleft;
   String x;
   String y;
   String z;
   Player p;
   private boolean cancelled;
   private static final HandlerList handlers = new HandlerList();

   public ScoreboardUpdateEvent(Player var1, String var2, String var3, String var4, String var5, String var6, String var7, String var8, String var9) {
      this.capper = var2;
      this.koth = var3;
      this.world = var4;
      this.faction = var5;
      this.timeleft = var6;
      this.x = var7;
      this.y = var8;
      this.z = var9;
      this.p = var1;
   }

   public boolean isCancelled() {
      return this.cancelled;
   }

   public void setCancelled(boolean var1) {
      this.cancelled = var1;
   }

   public String getCapper() {
      return this.capper;
   }

   public String getFaction() {
      return this.faction;
   }

   public String getKOTHName() {
      return this.koth;
   }

   public String getX() {
      return this.x;
   }

   public String getY() {
      return this.y;
   }

   public String getZ() {
      return this.z;
   }

   public String getWorld() {
      return this.world;
   }

   public String getTimeRemaining() {
      return this.timeleft;
   }

   public Player getPlayer() {
      return this.p;
   }

   public HandlerList getHandlers() {
      return handlers;
   }

   public static HandlerList getHandlerList() {
      return handlers;
   }
}
