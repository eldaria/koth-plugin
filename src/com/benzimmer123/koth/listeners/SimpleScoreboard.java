package com.benzimmer123.koth.listeners;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Team;

import java.util.AbstractMap.SimpleEntry;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SimpleScoreboard {
   private org.bukkit.scoreboard.Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
   private String title;
   private Map<String, Integer> scores;
   private List<Team> teams;

   public SimpleScoreboard(String var1) {
      this.title = var1;
      this.scores = Maps.newLinkedHashMap();
      this.teams = Lists.newArrayList();
   }

   public void blankLine() {
      this.add(" ");
   }

   public void add(String var1) {
      this.add(var1, (Integer)null);
   }

   public void add(String var1, Integer var2) {
      Preconditions.checkArgument(var1.length() < 48, "text cannot be over 48 characters in length");
      var1 = this.fixDuplicates(var1);
      this.scores.put(var1, var2);
   }

   private String fixDuplicates(String var1) {
      while(this.scores.containsKey(var1)) {
         var1 = var1 + "§r";
      }

      if (var1.length() > 48) {
         var1 = var1.substring(0, 47);
      }

      return var1;
   }

   private Entry createTeam(String var1) {
      String var2 = "";
      if (var1.length() <= 16) {
         return new SimpleEntry((Object)null, var1);
      } else {
         Team var3 = this.scoreboard.registerNewTeam("text-" + this.scoreboard.getTeams().size());
         Iterator var4 = Splitter.fixedLength(16).split(var1).iterator();
         var3.setPrefix((String)var4.next());
         var2 = (String)var4.next();
         if (var1.length() > 32) {
            var3.setSuffix((String)var4.next());
         }

         this.teams.add(var3);
         return new SimpleEntry(var3, var2);
      }
   }

   public void build() {
      Objective var1 = this.scoreboard.registerNewObjective(this.title.length() > 16 ? this.title.substring(0, 15) : this.title, "dummy");
      var1.setDisplayName(this.title);
      var1.setDisplaySlot(DisplaySlot.SIDEBAR);
      int var2 = this.scores.size();

      for(Entry<String, Integer> var3 : this.scores.entrySet()) {
         Integer var5 = var3.getValue() != null ? ((Integer)var3.getValue()).intValue() : var2;
         Entry var6 = this.createTeam((String)var3.getKey());
         String var7 = (String)var6.getValue();
         if (var6.getKey() != null) {
            ((Team)var6.getKey()).addEntry(var7);
         }

         var1.getScore(var7).setScore(var5.intValue());
         --var2;
      }

   }

   public void reset() {
      this.title = null;
      this.scores.clear();

      for(Team var1 : this.teams) {
         var1.unregister();
      }

      this.teams.clear();
   }

   public org.bukkit.scoreboard.Scoreboard getScoreboard() {
      return this.scoreboard;
   }

   public void send(Player... var1) {
      for(Player var2 : var1) {
         var2.setScoreboard(this.scoreboard);
      }

   }
}
