package com.benzimmer123.koth.listeners;

import com.benzimmer123.koth.P;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.List;

public class Scoreboard {
   P plugin;
   SimpleScoreboard scoreboard;

   public Scoreboard(P var1) {
      this.plugin = var1;
   }

   public void updateScoreboard(Player var1) {
      ScoreboardUpdateEvent var2 = new ScoreboardUpdateEvent(var1, (new Variables(this.plugin)).getCapper(), (new Variables(this.plugin)).getActiveKoth(), (new Variables(this.plugin)).getWorld(), (new Variables(this.plugin)).getFaction(), (new Variables(this.plugin)).getFormattedTime(), (new Variables(this.plugin)).getX(), (new Variables(this.plugin)).getY(), (new Variables(this.plugin)).getZ());
      Bukkit.getServer().getPluginManager().callEvent(var2);
      if (!var2.isCancelled() && this.plugin.settings.getConfig().getBoolean("SCOREBOARD.USE_SCOREBOARD")) {
         List var3 = this.plugin.settings.getConfig().getList("SCOREBOARD.DISABLE_IN_WORLD");
         if (var3 == null || !var3.contains(var1.getWorld().getName().toUpperCase())) {
            this.scoreboard = new SimpleScoreboard(this.plugin.settings.getConfig().getString("SCOREBOARD.SCOREBOARD_TITLE").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&k", "§k").replaceAll("&r", "§r").replaceAll("&n", "§n").replaceAll("&m", "§m"));

            for(String var5 : this.plugin.settings.getConfig().getStringList("SCOREBOARD.SCOREBOARD_LINES")) {
               if (var5 != " ") {
                  this.scoreboard.add(var5.replaceAll("%player%", (new Variables(this.plugin)).getCapper()).replaceAll("%world%", (new Variables(this.plugin)).getWorld()).replaceAll("%x%", (new Variables(this.plugin)).getX()).replaceAll("%y%", (new Variables(this.plugin)).getY()).replaceAll("%z%", (new Variables(this.plugin)).getZ()).replaceAll("%koth%", (new Variables(this.plugin)).getActiveKothName()).replaceAll("%timeleft%", (new Variables(this.plugin)).getFormattedTime()).replaceAll("%secondsleft%", String.valueOf((new Variables(this.plugin)).getSecondsRemaining())).replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&k", "§k").replaceAll("&r", "§r").replaceAll("&n", "§n").replaceAll("&m", "§m").replaceAll("%faction%", (new Variables(this.plugin)).getFaction()));
               } else {
                  this.scoreboard.blankLine();
               }
            }

            this.scoreboard.build();
            this.scoreboard.send(var1);
         }
      }

   }

   public void removeScoreboard() {
      for(Player var1 : this.plugin.getServer().getOnlinePlayers()) {
         if (var1.getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null && var1.getScoreboard().getObjective(this.plugin.settings.getConfig().getString("SCOREBOARD.SCOREBOARD_TITLE").replaceAll("(&([a-f0-9]))", "§$2").replaceAll("&l", "§l").replaceAll("&o", "§o").replaceAll("&k", "§k").replaceAll("&r", "§r").replaceAll("&n", "§n").replaceAll("&m", "§m")) != null) {
            var1.getScoreboard().getObjective(DisplaySlot.SIDEBAR).unregister();
         }
      }

   }
}
